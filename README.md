# 💻 Tugas Kelompok

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Kelas**: J | **Kode Tutor**: RF

Repositori ini adalah tempat pengerjaan Tugas Kelompok untuk mata kuliah Dasar-Dasar Pemrograman 2 Semester Gasal 2020/2021.

## Cara Penggunaan

1. Buatlah satu proyek GitLab dengan nama **\[Kode Tutor\]-DDP2-TK** (Contoh: **RF-DDP2-TK**) di akun salah satu anggota kelompok dengan visibilitas **Private**. Jangan lupa mengundang sesama anggota kelompok.

2. Buatlah salinan proyek tersebut di komputermu dengan menggunakan <code>git clone</code>.

3. Tambahkan repositori ini sebagai *upstream* pada salinan proyek di komputermu tadi dengan menggunakan instruksi berikut ini:

   ~~~shell
   git remote add upstream https://gitlab.com/DDP2-CSUI/2020-gasal/group-assignments.git
   ~~~

4. Pada setiap rilis tugas pemrograman, repositori ini akan diperbarui. Untuk mengintegrasikan perubahan tersebut dengan proyekmu, gunakan instruksi berikut ini:

   ~~~shell
   git pull --allow-unrelated-histories upstream master
   ~~~

## Checklist

Untuk mengisi *checklist* ini, kamu dapat membuka berkas README.md melalui Visual Studio Code dan mengganti <code>[ ]</code> dengan <code>[X]</code>. GitLab akan secara otomatis mengolahnya sebagai sebuah *checklist*.

- [ ] Tugas Pemrograman 3:  I really enjoy programming, I repeatedly do it.
- [ ] Tugas Pemrograman 4: Let’s solve the problem bit by bit.
- [ ] Tugas Pemrograman 5: We are stronger as one. 
- [ ] Tugas Pemrograman 7: What a picture it is, such a beautiful code abstraction
- [ ] Tugas Pemrograman 8: They are my children. They inherited my methods and properties. Although they may behave differently. I'm still proud of them!
- [ ] Tugas Pemrograman 9: Oops, what is going on in here?
- [ ] Tugas Pemrograman 11: TP11: Wow, graphics are good !
- [ ] Tugas Pemrograman 12: Yeay, We’ve made a nice GUI program !
- [ ] Tugas Pemrograman 13: We’ve learned this kind of problem before, but why generics?


## License

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2020, Faculty of Computer Science Universitas Indonesia

Permission to copy, modify, and share the works in this project are governed under the [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause) license.