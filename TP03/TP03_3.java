import java.util.Random;
import java.util.Scanner;

public class TP03_3 {
    public static void main(String[] args) {
        // Konstanta
        final int STATUS_BENAR = 1;
        final int STATUS_SALAH = 0;
        final int MAX_PENGUNJUNG = 5;
        
        // Variabel Umum
        Random randomizer = new Random();
        Scanner sc = new Scanner(System.in);
        char specialChar = '1';
        String nama = "";
        String password = "";
        int jumlahPengunjung = 0;
        
        System.out.println("Selamat datang di Sistem Gerbang Kedai Kopi VoidMain!");
        
        // TODO: Kerjakan logika penentuan karakter spesial di sini

        // Jika dicetak masih 1, maka program pengecekan karaktermu belum tepat :)
        System.out.println("Karakter spesial hari ini adalah: "+specialChar);

        System.out.println("=====PELAYANAN PENGUNJUNG======");
        System.out.println("Maksimum pengunjung saat pandemi: "+MAX_PENGUNJUNG);
        System.out.println("Untuk keluar, ketik 'exit'");
        System.out.println("===============================");

        // TODO: Kerjakan logika pengecekan nama dan kata sandi pengunjung di close

        System.out.println("Jumlah pengunjung kedai kopi: "+jumlahPengunjung+" orang");
        System.out.println("Hari selesai!");
        sc.close(); // PENTING: Scanner perlu di-close setelah tidak digunakan.
    }
}