import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
public class Kedai{  // TODO: Modifikasi kelas Restoran sehingga Simulator.java bisa berjalan
    private Burhan burhan;
    private double kas;
    private double idxK;
    private Queue<Pelanggan> antrian = new LinkedList<>();
    private Map<String, Integer> menu;
    public Kedai(double kas, String menu){   
        this.burhan = new Burhan(10000.0);
        this.kas = kas;
        this.idxK = 100.0;
        parseMenu(menu);
    }

    public void parseMenu(String menu){  // TODO: Implementasikan method untuk memasukkan menu dari file txt menjadi object!
        // Hint: cek Seeder.java :D
        this.menu = new HashMap<>();
    }

    public void layaniPelanggan(){  // TODO: Implementasikan method untuk melayani semua pelanggan di antrian!
        
    }
    public void reset() { // TODO: Implementasikan method untuk mereset kedai sesuai permintaan soal!

    }

    public void tutup() { // TODO: Implementasikan method untuk rekap total pendapatan secara keseluruhan!

    }

    public Burhan getBurhan() {
        return this.burhan;
    }

    public Queue<Pelanggan> getAntrian() {
        return this.antrian;
    }

    public String getDaftarAntrian() {
        return Arrays.toString(this.antrian.toArray());
    }

    public Pelanggan getPelangganTerdepan() {
        return this.antrian.poll();
    }

    public void tambahAntrianPelanggan(Pelanggan p){
        this.antrian.add(p);
    }

    public double getKas() {
        return this.kas;
    }

    public void setKas(double kas) {
        this.kas = kas;
    }

    public double getIdxK(){
        return this.idxK;
    }

    public void setIdxK(double idxK) {
        this.idxK = idxK;
    }

    @Override
    public String toString() {
        return "Kedai voidMain, dijaga solo oleh Kak Burhan. Total uang kas sekarang ="+Double.toString(kas);
    }
    
    public static void print(String s) {
        System.out.println(s);
    }
    public static void print(int i) {
        System.out.println(i);
    }
    public static void print(double i) {
        System.out.println(i);
    }

}