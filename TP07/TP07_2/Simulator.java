import Burhan;

import java.util.Scanner;

import Bon;
import Pelanggan;
import Restoran;
import Seeder;

public class Simulator {    // NOTE: Silakan gunakan template yang diberikan sesuai keinginan dan kepercayaan masing-masing.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Hardcoded Testcase, hasil tergantung interaksi pelanggan.
        Kedai voidMain = new Kedai(0.0);
        Burhan burhan = voidMain.getBurhan();
        voidMain.tambahAntrianPelanggan(new Pelanggan("A", 20000.0));
        voidMain.tambahAntrianPelanggan(new Pelanggan("B", 20000.0));
        voidMain.layaniPelanggan();
        burhan.bersihkan(voidMain);
        burhan.setUang(100000.0);
        voidMain.tambahAntrianPelanggan(new Pelanggan("C", 50000.0));
        voidMain.tambahAntrianPelanggan(new Pelanggan("D", 50000.0));
        voidMain.layaniPelanggan();
        for (int i = 1; i < 4; i++) {
            voidMain.tambahAntrianPelanggan(new Pelanggan(Integer.toString(i), 50000.0));
        }
        voidMain.layaniPelanggan();
        voidMain.tutup();
        
        // ======================Batas Hardcoded Test Case==========================================

        // reset
        voidMain.reset();
        
        // ==============================User Input===============================================
        String perintah;
        do {
            perintah = scanner.nextLine();
            /**
             * Lengkapi switch case berikut supaya bisa menjalankan 
             * semua perintah yang diminta oleh soal!
             */
            switch (perintah) { 
                case "value":
                    
                    break;
            
                default:
                    break;
            }
        } while (perintah.equalsIgnoreCase("Tutup Kedai"));
        voidMain.tutup();
    }
    public static void print(String s) {
        System.out.println(s);
    }
    public static void print(int i) {
        System.out.println(i);
    }
    public static void print(double i) {
        System.out.println(i);
    }
}
