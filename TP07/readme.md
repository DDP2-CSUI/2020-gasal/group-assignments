# 💻 Tugas Pemrograman 7 (Kelompok)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Kelas**:  | **Kode Tutor**: 

## Topik

What a picture it is, such a beautiful code abstraction 

## Dokumen Tugas

https://docs.google.com/document/d/1ejZMj23ZNub5jeEjnO3v_zQn9P4-ZPLpsmc0e3ATu2k/

## Pembagian Tugas

| No.  | Nama | NPM  | Tugas |
| ---- | ---- | ---- | ----- |
| 1    |      |      |       |
| 2    |      |      |       |
| 3    |      |      |       |
| 4    |      |      |       |
| 5    |      |      |       |
| 6    |      |      |       |
| 7    |      |      |       |
| 8    |      |      |       |

## *Checklist*

- [ ] Membaca dan memahami Soal 1
- [ ] Membaca dan memahami Soal 2
- [ ] Membaca dan memahami Soal 3
- [ ] Mengerjakan Soal 2
- [ ] Mengerjakan Soal 3
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 1
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 2
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 3
- [ ] Melakukan demo secara kolektif